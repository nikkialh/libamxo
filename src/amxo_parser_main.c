/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <sys/resource.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <libgen.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include "amxo_assert.h"
#include "amxo_parser_priv.h"
#include "amxo_parser_hooks_priv.h"
#include "amxo_parser.tab.h"

static ssize_t amxo_parser_string_reader(amxo_parser_t *parser,
                                         void *buf,
                                         size_t max_size) {
    ssize_t result = 0;
    result = amxc_rbuffer_read(&parser->rbuffer, buf, max_size);
    errno = 0;

    return result;
}

static int amxo_parser_parse_fd_internal(amxo_parser_t *parser,
                                         int fd,
                                         amxd_object_t *object) {
    int retval = -1;

    parser->fd = fd;
    parser->object = object;
    parser->reader = amxo_parser_fd_reader;

    amxo_parser_create_lex(parser);
    retval = yyparse(parser->scanner);
    amxo_parser_destroy_lex(parser);

    parser->fd = -1;
    parser->object = NULL;

    return retval;
}

static void amxo_parser_entry_point_free(amxc_llist_it_t *it) {
    amxo_entry_t *entry = amxc_llist_it_get_data(it, amxo_entry_t, it);
    free(entry);
}

static void amxo_parser_connection_free(amxc_llist_it_t *it) {
    amxo_connection_t *con = amxc_llist_it_get_data(it, amxo_connection_t, it);
    free(con);
}


ssize_t AMXO_PRIVATE amxo_parser_fd_reader(amxo_parser_t *parser,
                                           void *buf,
                                           size_t max_size) {
    ssize_t result = 0;
    errno = 0;
    result = read(parser->fd, buf, max_size);
    if((result == -1) && (errno != EAGAIN)) {
        printf("Read failed %d\n", errno);
    }
    errno = 0;

    return result;
}

int AMXO_PRIVATE amxo_parser_parse_file_impl(amxo_parser_t *parser,
                                             const char *file_path,
                                             amxd_object_t *object) {
    int retval = -1;
    int fd = -1;

    fd = open(file_path, O_RDONLY);
    when_true(fd == -1, exit);
    parser->file = file_path;
    retval = amxo_parser_parse_fd_internal(parser, fd, object);
    close(fd);

exit:
    return retval;
}

void AMXO_PRIVATE amxo_parser_child_init(amxo_parser_t *parser) {
    when_null(parser, exit);

    parser->fd = -1;
    parser->object = NULL;
    parser->param = NULL;
    parser->func = NULL;
    parser->status = 0;
    parser->resolved_fn = NULL;
    parser->resolvers = NULL;
    parser->include_stack = NULL;
    parser->hooks = NULL;
    parser->entry_points = NULL;
    parser->connections = NULL;
    parser->data = NULL;

    amxc_rbuffer_init(&parser->rbuffer, 0);
    amxc_string_init(&parser->msg, 0);
    amxc_astack_init(&parser->object_stack);
    amxc_var_init(&parser->config);

    parser->file = "<unknown>";

exit:
    return;
}

int amxo_parser_init(amxo_parser_t *parser) {
    int retval = -1;
    amxc_var_t *config = NULL;
    when_null(parser, exit);

    amxo_parser_child_init(parser);

    amxc_var_set_type(&parser->config, AMXC_VAR_ID_HTABLE);
    config = amxc_var_add_key(amxc_llist_t, &parser->config, "include-dirs", NULL);
    amxc_var_add(cstring_t, config, ".");

    amxo_parser_init_resolvers(parser);

    retval = 0;

exit:
    return retval;
}

void amxo_parser_clean(amxo_parser_t *parser) {
    when_null(parser, exit);

    parser->fd = -1;
    parser->object = NULL;
    parser->param = NULL;
    parser->func = NULL;
    parser->status = 0;
    parser->resolved_fn = NULL;

    amxc_rbuffer_clean(&parser->rbuffer);
    amxc_string_clean(&parser->msg);
    amxc_astack_clean(&parser->object_stack, NULL);

    amxo_parser_clean_resolvers(parser);
    if(parser->resolvers != NULL) {
        amxc_htable_delete(&parser->resolvers, NULL);
    }

    amxc_llist_delete(&parser->hooks, NULL);
    amxc_var_clean(&parser->config);
    amxc_var_delete(&parser->include_stack);
    amxc_llist_delete(&parser->entry_points, amxo_parser_entry_point_free);
    amxc_llist_delete(&parser->connections, amxo_parser_connection_free);

exit:
    return;
}

int amxo_parser_new(amxo_parser_t **parser) {
    int retval = -1;
    when_null(parser, exit);

    *parser = calloc(1, sizeof(amxo_parser_t));
    when_null((*parser), exit);

    retval = amxo_parser_init(*parser);

exit:
    if((retval != 0) && (*parser != NULL)) {
        amxo_parser_clean(*parser);
        free(*parser);
        *parser = NULL;
    }
    return retval;
}

void amxo_parser_delete(amxo_parser_t **parser) {
    when_null(parser, exit);
    when_null(*parser, exit);

    amxo_parser_clean(*parser);
    free(*parser);
    *parser = NULL;

exit:
    return;
}

int amxo_parser_parse_fd(amxo_parser_t *parser,
                         int fd,
                         amxd_object_t *object) {
    int retval = -1;
    struct rlimit nofile = { 0, 0 };
    when_null(parser, exit);
    when_null(object, exit);

    when_failed(getrlimit(RLIMIT_NOFILE, &nofile), exit);

    when_true(fd < 0 || (rlim_t) llabs(fd) > nofile.rlim_max, exit);
    when_failed(fcntl((int) llabs(fd), F_GETFD), exit);

    amxo_hooks_start(parser);
    retval = amxo_parser_parse_fd_internal(parser, fd, object);
    amxo_hooks_end(parser);

exit:
    return retval;
}

int amxo_parser_parse_file(amxo_parser_t *parser,
                           const char *file_path,
                           amxd_object_t *object) {
    int retval = -1;
    char *current_wd = getcwd(NULL, 0);
    char *real_path = NULL;
    char *dir_name = NULL;
    when_str_empty(file_path, exit);

    real_path = realpath(file_path, NULL);
    if(real_path != NULL) {
        dir_name = dirname(real_path);
        when_true(chdir(dir_name) == -1, exit);
        dir_name[strlen(dir_name)] = '/';
    }

    parser->file = real_path == NULL ? file_path : real_path;

    amxo_hooks_start(parser);
    retval = amxo_parser_parse_file_impl(parser,
                                         parser->file,
                                         object);
    amxo_hooks_end(parser);

    if(real_path != NULL) {
        when_true(chdir(current_wd) == -1, exit);
    }

exit:
    free(current_wd);
    free(real_path);
    return retval;
}

int amxo_parser_parse_string(amxo_parser_t *parser,
                             const char *text,
                             amxd_object_t *object) {
    int retval = -1;
    when_null(parser, exit);
    when_null(object, exit);
    when_str_empty(text, exit);

    amxc_rbuffer_write(&parser->rbuffer, text, strlen(text));
    parser->object = object;
    parser->reader = amxo_parser_string_reader;

    amxo_parser_create_lex(parser);
    retval = yyparse(parser->scanner);
    amxo_parser_destroy_lex(parser);

    amxc_rbuffer_clean(&parser->rbuffer);
    parser->object = NULL;

exit:
    return retval;
}

amxc_var_t *amxo_parser_get_config(amxo_parser_t *parser,
                                   const char *name) {
    amxc_var_t *retval = NULL;
    when_null(parser, exit);
    when_str_empty(name, exit);

    retval = amxc_var_get_key(&parser->config, name, AMXC_VAR_FLAG_DEFAULT);
    if(retval == NULL) {
        retval = amxc_var_add_new_key(&parser->config, name);
    }

exit:
    return retval;
}

int amxo_parser_set_config(amxo_parser_t *parser,
                           const char *name,
                           amxc_var_t *value) {
    int retval = 0;
    when_null(parser, exit);
    when_null(value, exit);
    when_str_empty(name, exit);

    retval = amxc_var_set_key(&parser->config,
                              name,
                              value,
                              AMXC_VAR_FLAG_UPDATE | AMXC_VAR_FLAG_COPY);

exit:
    return retval;
}

int amxo_parser_add_entry_point(amxo_parser_t *parser,
                                amxo_entry_point_t fn) {
    int retval = -1;
    amxo_entry_t *ep = NULL;
    when_null(parser, exit);
    when_null(fn, exit);

    if(parser->entry_points == NULL) {
        retval = amxc_llist_new(&parser->entry_points);
        when_null(parser->entry_points, exit);
    }

    amxc_llist_for_each(it, parser->entry_points) {
        ep = amxc_llist_it_get_data(it, amxo_entry_t, it);
        if(ep->entry_point == fn) {
            retval = 0;
            goto exit;
        }
    }

    ep = calloc(1, sizeof(amxo_entry_t));
    when_null(ep, exit);

    ep->entry_point = fn;
    amxc_llist_append(parser->entry_points, &ep->it);
    retval = 0;

exit:
    if(retval != 0) {
        free(ep);
    }
    return retval;
}

int amxo_parser_invoke_entry_points(amxo_parser_t *parser,
                                    amxd_dm_t *dm,
                                    int reason) {
    int retval = -1;
    int fail_count = 0;
    when_null(parser, exit);
    when_null(dm, exit);
    if(parser->entry_points == NULL) {
        retval = 0;
        goto exit;
    }

    amxc_llist_for_each(it, parser->entry_points) {
        amxo_entry_t *ep = amxc_llist_it_get_data(it, amxo_entry_t, it);
        retval = ep->entry_point(reason, dm, parser);
        if(retval != 0) {
            fail_count++;
        }
    }

    retval = fail_count;

exit:
    return retval;
}

int amxo_connection_add(amxo_parser_t *parser,
                        int fd,
                        amxo_fd_read_t reader,
                        const char *uri,
                        amxo_con_type_t type,
                        void *priv) {
    int retval = -1;
    amxo_connection_t *con = NULL;
    amxc_var_t var_fd;

    amxc_var_init(&var_fd);
    when_null(parser, exit);
    when_null(reader, exit);
    when_true(fd < 0, exit);

    if(parser->connections == NULL) {
        retval = amxc_llist_new(&parser->connections);
        when_null(parser->connections, exit);
    }

    amxc_llist_for_each(it, parser->connections) {
        con = amxc_llist_it_get_data(it, amxo_connection_t, it);
        if(con->fd == fd) {
            retval = 0;
            goto exit;
        }
    }

    con = calloc(1, sizeof(amxo_connection_t));
    when_null(con, exit);

    con->uri = uri;
    con->fd = fd;
    con->priv = priv;
    con->reader = reader;
    con->type = type;
    amxc_llist_append(parser->connections, &con->it);

    amxc_var_set(fd_t, &var_fd, fd);
    amxp_sigmngr_trigger_signal(NULL, "connection-added", &var_fd);
    retval = 0;

exit:
    if(retval != 0) {
        free(con);
    }
    amxc_var_clean(&var_fd);
    return retval;

}

int amxo_connection_remove(amxo_parser_t *parser,
                           int fd) {
    int retval = -1;
    amxo_connection_t *con = NULL;
    when_null(parser, exit);
    when_null(parser->connections, exit);

    amxc_llist_for_each(it, parser->connections) {
        con = amxc_llist_it_get_data(it, amxo_connection_t, it);
        if(con->fd == fd) {
            amxc_var_t var_fd;
            amxc_var_set(fd_t, &var_fd, fd);
            amxp_sigmngr_trigger_signal(NULL, "connection-added", &var_fd);
            amxc_var_clean(&var_fd);
            amxc_llist_it_clean(&con->it, amxo_parser_connection_free);
            break;
        }
    }

    if(amxc_llist_is_empty(parser->connections)) {
        amxc_llist_delete(&parser->connections, NULL);
    }

    retval = 0;

exit:
    return retval;
}

amxo_connection_t *amxo_connection_get_first(amxo_parser_t *parser,
                                             amxo_con_type_t type) {
    amxo_connection_t *con = NULL;
    when_null(parser, exit);

    amxc_llist_for_each(it, parser->connections) {
        con = amxc_llist_it_get_data(it, amxo_connection_t, it);
        if(con->type == type) {
            break;
        }
        con = NULL;
    }

exit:
    return con;
}

amxo_connection_t *amxo_connection_get_next(amxo_parser_t *parser,
                                            amxo_connection_t *con,
                                            amxo_con_type_t type) {
    amxc_llist_it_t *it = NULL;
    when_null(parser, exit);
    when_null(con, exit);
    when_true(con->it.llist != parser->connections, exit);

    it = amxc_llist_it_get_next(&con->it);
    while(it) {
        con = amxc_llist_it_get_data(it, amxo_connection_t, it);
        if(con->type == type) {
            break;
        }
        con = NULL;
        it = amxc_llist_it_get_next(it);
    }

exit:
    return con;
}

int amxo_connection_set_el_data(amxo_parser_t *parser,
                                int fd,
                                void *el_data) {
    int retval = -1;
    amxo_connection_t *con = NULL;
    when_null(parser, exit);
    when_null(parser->connections, exit);

    amxc_llist_for_each(it, parser->connections) {
        con = amxc_llist_it_get_data(it, amxo_connection_t, it);
        if(con->fd == fd) {
            con->el_data = el_data;
            break;
        }
    }

    retval = 0;

exit:
    return retval;
}
